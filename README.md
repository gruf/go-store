KVStore: a simple, highly perform key-value store with multiple storage backends.

The underlying storage method for this store can be handled by:

    - DiskStorage: storage implementation that stores directly to a filesystem, with optional settable key <--> filepath transform function

    - S3Storage: storage implementation that stores values in an S3 instance

    - MemoryStorage: storage implementation that is essentially just an in-memory hashmap

    - Any other type implementing storage.Storage{}

DiskStorage and BlockStorage support compression by implementing a `Compressor` interface, with default gzip, zlib and snappy implementations.