module codeberg.org/gruf/go-store/v2

go 1.22.2

toolchain go1.22.3

require (
	codeberg.org/gruf/go-bytes v1.0.2
	codeberg.org/gruf/go-fastcopy v1.1.2
	codeberg.org/gruf/go-fastpath/v2 v2.0.0
	codeberg.org/gruf/go-iotools v0.0.0-20230811115124-5d4223615a7f
	codeberg.org/gruf/go-mutexes v1.5.0
	github.com/cornelk/hashmap v1.0.8
	github.com/klauspost/compress v1.17.8
	github.com/minio/minio-go/v7 v7.0.70
)

require (
	codeberg.org/gruf/go-mempool v0.0.0-20240507125005-cef10d64a760 // indirect
	github.com/dolthub/maphash v0.1.0 // indirect
	github.com/dolthub/swiss v0.2.1 // indirect
	github.com/dustin/go-humanize v1.0.1 // indirect
	github.com/goccy/go-json v0.10.2 // indirect
	github.com/google/uuid v1.6.0 // indirect
	github.com/klauspost/cpuid/v2 v2.2.6 // indirect
	github.com/minio/md5-simd v1.1.2 // indirect
	github.com/rs/xid v1.5.0 // indirect
	golang.org/x/crypto v0.21.0 // indirect
	golang.org/x/net v0.23.0 // indirect
	golang.org/x/sys v0.18.0 // indirect
	golang.org/x/text v0.14.0 // indirect
	gopkg.in/ini.v1 v1.67.0 // indirect
)
