package storage_test

import (
	"bytes"
	"io"
	"testing"

	"codeberg.org/gruf/go-iotools"
	"codeberg.org/gruf/go-store/v2/storage"
)

func TestGZipCompressor(t *testing.T) {
	testCompressor(t, storage.GZipCompressor())
}

func TestZLibCompressor(t *testing.T) {
	testCompressor(t, storage.ZLibCompressor())
}

func TestSnappyCompressor(t *testing.T) {
	testCompressor(t, storage.SnappyCompressor())
}

func testCompressor(t *testing.T, c storage.Compressor) {
	testCompressorString(t, c, "hello world")
	testCompressorString(t, c, "hello world\nwith\nnewlines\n")
	testCompressorString(t, c, string([]byte{0, 1, 2, 3, 4}))
}

func testCompressorString(t *testing.T, c storage.Compressor, input string) {
	buf := bytes.NewBuffer(nil)

	wc, err := c.Writer(iotools.NopWriteCloser(buf))
	if err != nil {
		t.Errorf("Failed creating compression writer: %v", err)
		return
	}

	if _, err := io.Copy(wc, bytes.NewReader([]byte(input))); err != nil {
		t.Errorf("Failed writing compressed output: %v", err)
		return
	}

	if err := wc.Close(); err != nil {
		t.Errorf("Failed closing compression writer: %v", err)
		return
	}

	rc, err := c.Reader(iotools.NopReadCloser(buf))
	if err != nil {
		t.Errorf("Failed creating compression reader: %v", err)
		return
	}

	out, err := io.ReadAll(rc)
	if err != nil {
		t.Errorf("Failed reading compressed input: %v", err)
		return
	}

	if err := rc.Close(); err != nil {
		t.Errorf("Failed closing compression reader: %v", err)
		return
	}

	if string(out) != input {
		t.Errorf("Compressed writing followed by reading did not produce same result\nexpect=%q\nresult=%q\n", input, out)
		return
	}
}
