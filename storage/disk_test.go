package storage_test

import (
	"context"
	"math/rand"
	"os"
	"path"
	"strconv"
	"testing"

	"codeberg.org/gruf/go-store/v2/storage"
)

func TestDiskStorage(t *testing.T) {
	// Set test path, defer deleting it
	testPath := "diskstorage.test"
	t.Cleanup(func() {
		os.RemoveAll(testPath)
	})

	// Open new diskstorage instance
	st, err := storage.OpenDisk(testPath, nil)
	if err != nil {
		t.Fatalf("Failed opening storage: %v", err)
	}

	// Attempt multi open of same instance
	_, err = storage.OpenDisk(testPath, nil)
	if err == nil {
		t.Fatal("Successfully opened a locked storage instance")
	}

	// Run the storage tests
	// (this closes it)
	testStorage(t, st)

	// Test reopen storage path
	st, err = storage.OpenDisk(testPath, nil)
	if err != nil {
		t.Fatalf("Failed opening storage: %v", err)
	}
	st.Close()
}

func TestDiskStorageDirTraversal(t *testing.T) {
	for _, path := range []string{
		"diskstorage.test",
		"./diskstorage.test",
		"/tmp/diskstorage.test",
		"/tmp/../tmp/diskstorage.test",
	} {
		testDiskStorageDirTraversal(t, path)
	}
}

func testDiskStorageDirTraversal(t *testing.T, path string) {
	ctx := context.Background()

	// Delete path on exit
	t.Cleanup(func() {
		os.RemoveAll(path)
	})

	// Open new diskstorage instance
	st, err := storage.OpenDisk(path, nil)
	if err != nil {
		t.Fatalf("Failed opening storage: %v", err)
	}
	defer st.Close()

	// Attempt directory traversals from current disk path
	if _, err := st.Stat(ctx, "../file.txt"); err != storage.ErrInvalidKey {
		t.Fatal("directory traversal did not result in error")
	}
	if _, err := st.Stat(ctx, "./../file.txt"); err != storage.ErrInvalidKey {
		t.Fatal("directory traversal did not result in error")
	}
	if _, err := st.Stat(ctx, "../../../file.txt"); err != storage.ErrInvalidKey {
		t.Fatal("directory traversal did not result in error")
	}
	if _, err := st.Stat(ctx, "./../dir/../file.txt"); err != storage.ErrInvalidKey {
		t.Fatal("directory traversal did not result in error")
	}
}

func TestDiskStorageCustomLockfile(t *testing.T) {
	// Set test path, defer deleting it
	testPath := "diskstorage.test"
	lockfile := path.Join(os.TempDir(), tmpFileName())
	cfg := storage.DiskConfig{
		LockFile: lockfile,
	}
	t.Cleanup(func() {
		os.RemoveAll(testPath)
		os.Remove(lockfile)
	})

	// Open new diskstorage instance
	st, err := storage.OpenDisk(testPath, &cfg)
	if err != nil {
		t.Fatalf("Failed opening storage: %v", err)
	}

	// Attempt multi open of same instance
	_, err = storage.OpenDisk(testPath, &cfg)
	if err == nil {
		t.Fatal("Successfully opened a locked storage instance")
	}

	// Run the storage tests
	testStorage(t, st)

	// Test reopen storage path
	st, err = storage.OpenDisk(testPath, &cfg)
	if err != nil {
		t.Fatalf("Failed opening storage: %v", err)
	}
	st.Close()
}

func TestDiskStorageClean(t *testing.T) {
	// Set test path, defer deleting it
	testPath := "diskstorage.test"
	t.Cleanup(func() {
		os.RemoveAll(testPath)
	})

	// Create empty dir in storage for disk clean to pickup
	if err := os.MkdirAll(testPath+"/store/empty", 0o755); err != nil {
		t.Fatalf("Failed creating empty storage dir: %v", err)
	}

	// Open new diskstorage instance
	st, err := storage.OpenDisk(testPath, nil)
	if err != nil {
		t.Fatalf("Failed opening storage: %v", err)
	}
	defer st.Close()

	// Clean this storage instance with empty dir
	if err := st.Clean(context.TODO()); err != nil {
		t.Fatalf("Failed cleaning storage instance: %v", err)
	}
}

// tmpFileName generates a random temp file name.
func tmpFileName() string {
	return "tmp." + strconv.FormatInt(rand.Int63(), 10)
}
