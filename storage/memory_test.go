package storage_test

import (
	"testing"

	"codeberg.org/gruf/go-store/v2/storage"
)

func TestMemoryStorage(t *testing.T) {
	// Open new memorystorage instance
	st := storage.OpenMemory(0, false)

	// Run the storage tests
	testStorage(t, st)
}
