package storage_test

import (
	"bytes"
	"context"
	"os"
	"testing"

	"codeberg.org/gruf/go-store/v2/storage"
	"github.com/minio/minio-go/v7"
	"github.com/minio/minio-go/v7/pkg/credentials"
)

func TestS3Storage(t *testing.T) {
	// Load test S3 instance info
	addr := os.Getenv("MINIO_ADDR")
	bucket := os.Getenv("MINIO_BUCKET")

	if addr == "" && bucket == "" {
		t.Skip("skipping S3Storage tests")
		return
	}

	// Attempt to open connection to S3 storage
	st, err := storage.OpenS3(addr, bucket, &storage.S3Config{
		CoreOpts: minio.Options{
			Creds: credentials.New(&credentials.EnvMinio{}),
		},
	})
	if err != nil {
		t.Fatalf("Failed opening storage: %v", err)
	}
	defer st.Close()

	// Run the storage tests
	testStorage(t, st)
}

func TestS3StorageChunkedWrite(t *testing.T) {
	// Load test S3 instance info
	addr := os.Getenv("MINIO_ADDR")
	bucket := os.Getenv("MINIO_BUCKET")

	if addr == "" && bucket == "" {
		t.Skip("skipping S3Storage tests")
		return
	}

	// Attempt to open connection to S3 storage
	st, err := storage.OpenS3(addr, bucket, &storage.S3Config{
		CoreOpts: minio.Options{
			Creds: credentials.New(&credentials.EnvMinio{}),
		},
	})
	if err != nil {
		t.Fatalf("Failed opening storage: %v", err)
	}
	defer st.Close()

	data := []byte("hello world\n1\n2\n3\n4\nfoobar\nbarfoo")

	// Stream this data into store (no ReaderSize, will be chunked)
	if _, err := st.WriteStream(context.TODO(), "data", bytes.NewReader(data)); err != nil {
		t.Fatalf("Failed chunked writing of data: %v", err)
	}

	// Read the written data back from store
	check, err := st.ReadBytes(context.TODO(), "data")
	if err != nil {
		t.Fatalf("Failed reading stored data: %v", err)
	}

	// Check that data is as expected
	if string(data) != string(check) {
		t.Fatal("Failed ensuring stored data matches input")
	}
}
