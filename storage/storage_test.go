package storage_test

import (
	"context"
	"errors"
	"fmt"
	"io"
	"testing"

	"codeberg.org/gruf/go-bytes"
	"codeberg.org/gruf/go-store/v2/storage"
)

func dataf(s string, a ...interface{}) []byte {
	return bytes.StringToBytes(fmt.Sprintf(s, a...))
}

func testStorage(t *testing.T, st storage.Storage) {
	expected := map[string][]byte{
		"test1": dataf("hello world!"),
		"test2": dataf("hello world!\nhelloworld!\nhelloworld!\nhewwoooo wooowddd\n"),
		"test3": dataf("i dunno some random data here %d", 64),
		"test4": dataf("aaaaaaaaaaaaaaaa asfj;asdflkjasd;kfnasdklf"),
	}

	// Write bytes first to give something for later reads
	t.Run("Storage.WriteBytes()", func(t *testing.T) {
		testStorageWriteBytes(t, st, "test1", expected["test1"])
		testStorageWriteBytes(t, st, "test2", expected["test2"])
	})
	t.Run("Storage.WriteStream()", func(t *testing.T) {
		testStorageWriteStream(t, st, "test3", expected["test3"])
		testStorageWriteStream(t, st, "test4", expected["test4"])
	})

	// Now attempt to read from previous tests
	t.Run("Storage.ReadBytes()", func(t *testing.T) {
		testStorageReadBytes(t, st, "test1", expected["test1"])
		testStorageReadBytes(t, st, "test2", expected["test2"])
	})
	t.Run("Storage.ReadStream()", func(t *testing.T) {
		testStorageReadStream(t, st, "test3", expected["test3"])
		testStorageReadStream(t, st, "test4", expected["test4"])
	})

	// Check that files from previous tests exist
	t.Run("Storage.Stat()", func(t *testing.T) {
		for key := range expected {
			testStorageStat(t, st, key)
		}
	})

	// Attempt to remove 1 of files from previous test
	t.Run("Storage.Remove()", func(t *testing.T) {
		testStorageRemove(t, st, "test1")
		delete(expected, "test1")
	})

	// Walk keys and check expected
	t.Run("Storage.WalkKeys()", func(t *testing.T) {
		expect := make([]string, 0, len(expected))
		for key := range expected {
			expect = append(expect, key)
		}
		testStorageWalkKeys(t, st, expect)
	})

	// We run clean last so there is something to clean
	t.Run("Storage.Clean()", func(t *testing.T) { testStorageClean(t, st) })

	// Test that closing storage works
	t.Run("Storage.Close()", func(t *testing.T) { testStorageClose(t, st) })
}

func testStorageClean(t *testing.T, st storage.Storage) {
	err := st.Clean(context.TODO())
	if err != nil {
		t.Fatalf("Error cleaning storage: %v", err)
	}
}

func testStorageReadBytes(t *testing.T, st storage.Storage, key string, value []byte) {
	b, err := st.ReadBytes(context.TODO(), key)
	if err != nil {
		t.Fatalf("Error reading from storage: %v", err)
	}
	if !bytes.Equal(b, value) {
		t.Fatalf("Error reading expected value '%s' from storage stream '%s'", value, b)
	}
	if _, err := st.ReadBytes(context.TODO(), key+"."); !errors.Is(err, storage.ErrNotFound) {
		t.Fatalf("Unexpected error reading non-existent file from storage: %v", err)
	}
}

func testStorageReadStream(t *testing.T, st storage.Storage, key string, value []byte) {
	r, err := st.ReadStream(context.TODO(), key)
	if err != nil {
		t.Fatalf("Error reading from storage: %v", err)
	}
	defer r.Close()
	b, err := io.ReadAll(r)
	if err != nil {
		t.Fatalf("Error reading from storage stream: %v", err)
	}
	if !bytes.Equal(b, value) {
		t.Fatalf("Error reading expected value '%s' from storage stream '%s'", value, b)
	}
	if _, err := st.ReadStream(context.TODO(), key+"."); !errors.Is(err, storage.ErrNotFound) {
		t.Fatalf("Unexpected error reading non-existent file from storage: %v", err)
	}
}

func testStorageWriteBytes(t *testing.T, st storage.Storage, key string, value []byte) {
	n, err := st.WriteBytes(context.TODO(), key, value)
	if err != nil {
		t.Fatalf("Error writing to storage: %v", err)
	} else if n != len(value) {
		t.Fatalf("Error writing to storage: did not write expected no. bytes")
	}
}

func testStorageWriteStream(t *testing.T, st storage.Storage, key string, value []byte) {
	n, err := st.WriteStream(context.TODO(), key, bytes.NewReader(value))
	if err != nil {
		t.Fatalf("Error writing to storage: %v", err)
	} else if int(n) != len(value) {
		t.Fatalf("Error writing to storage: did not write expected no. bytes")
	}
}

func testStorageStat(t *testing.T, st storage.Storage, key string) {
	ok, err := st.Stat(context.TODO(), key)
	if err != nil {
		t.Fatalf("Error stat'ing storage: %v", err)
	}
	if !ok {
		t.Fatalf("Error stat'ing expected file in storage")
	}
}

func testStorageRemove(t *testing.T, st storage.Storage, key string) {
	if err := st.Remove(context.TODO(), key); err != nil {
		t.Fatalf("Error removing from storage: %v", err)
	}
	if err := st.Remove(context.TODO(), key+"."); !errors.Is(err, storage.ErrNotFound) {
		t.Fatalf("Unexpected error removing non-existent file from storage: %#v", err)
	}
}

func testStorageWalkKeys(t *testing.T, st storage.Storage, expect []string) {
	remain := make([]string, len(expect))
	copy(remain, expect)

	err := st.WalkKeys(context.TODO(), storage.WalkKeysOptions{
		WalkFn: func(ctx context.Context, entry storage.Entry) error {
			key := entry.Key

			var i int
			for i = 0; i < len(remain); i++ {
				if key == remain[i] {
					break
				}
			}

			if i == len(remain) {
				return fmt.Errorf("Error finding unexpected key in storage '%s'", key)
			} else {
				remain = append(remain[:i], remain[i+1:]...)
			}

			return nil
		},
	})
	if err != nil {
		t.Fatalf("Error walking keys in storage: %v", err)
	}

	if len(remain) > 0 {
		t.Fatalf("Error finding expected keys in storage, '%v' not found", remain)
	}
}

func testStorageClose(t *testing.T, st storage.Storage) {
	err := st.Close()
	if err != nil {
		t.Fatalf("Error closing storage: %v", err)
	}
	_, err = st.ReadBytes(context.TODO(), "doesn't matter")
	if err != storage.ErrClosed {
		t.Fatalf("Did not get ErrClosed after closing storage: %v", err)
	}
}
